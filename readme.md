# Discodrive
A python discord bot to share switch games using gdrive.

## Google drive setup
The bot is made to be used with multiples drive account :
* 1 stash drive with all the games in 3 folder (base update and dlc)
* Multiples temp drives where copies of games will be made. Base, update and dlc folder from the stash drive need to be shared with each temp drive.
Each temp drive needs credential. 5 credentials are already on the git repo so the bot can support up to 5 temp drives. Just ask more credentials if needed.

## Bot setup
Go to config/config.txt to set up the bot.
There are multiples parameters, the first one (discord bot TOKEN) is required.


**Run setup_python.bat**

This script will installed required modules using pip.

**Run setup_drive.py**

This script will open a web browser to ask your google account permission to use the script. Accept it of course.
Then it will list all yout folders and subfolders in your drive and ask you wich are the base,update, dlc and shared folder.
Respond by giving the number corresponding to the right folder

## Launching the bot
Run bot.py
The first run takes time cause the bot will fetch a list of all your games in the differents folders and store their names and drive id in text files in the config folder.

When the bot is ready it'll show you in the python console.

## Usage
User can type the help command for help.

Admin only commands :
* update_list to update the list of games. Need to be run each time a game is added to stash
* update_config to got the bot parameters up to date with the config.txt file
* test_mat, test_games, test_params for troubleshoot purpose. test_params can be run to know what are the current bot parameters

## How it works

User ask for a game, bot searches for the game in the lists and asks user to choose the game from a list of games found.
When the game is chosen, the file is copied to the root of a temp drive. If there's no more space available, it's copied on the next temp drive and so on... 
Bot will then send a click limited short url which give access to the copy of the game.
If multiples users ask the same game, the bot will give the same url until there is only url_min_cliks clicks left on the short url.

Each check_files minutes, the bot look for expired files and delete them from their drive.

## Just enjoy