#script is in 3 big part : bot (to run discord bot), search (the differents step to list the games to download), drive (for all the drive api functions)
from init import *
import datetime
import asyncio
import requests as rq
import discord
from discord.ext import commands
import traceback
import time
import sys

####################Global Parameters##################
api_short='http://ulvis.net/API/write/get'
api_read='http://ulvis.net/API/read/get'

#general parameters
def init_params() :
    file=open('config/config.txt','r')
    cfg=file.read().split()
    file.close()
    
    TOKEN=cfg[cfg.index('TOKEN')+2]
    prefix=cfg[cfg.index('prefix')+2]
    check_files = float(cfg[cfg.index('check_files')+2])*60
    file_expire = float(cfg[cfg.index('file_expire')+2])
    url_max_clicks = int(cfg[cfg.index('url_max_clicks')+2])
    url_min_clicks =int(cfg[cfg.index('url_min_clicks')+2])
    color_embed = int(cfg[cfg.index('color_embed')+2],16)
    show_updates = int(cfg[cfg.index('show_updates')+2])
    update_channel = int(cfg[cfg.index('update_channel')+2])
    colors_list = [int(cfg[cfg.index('color_base')+2],16),int(cfg[cfg.index('color_update')+2],16),int(cfg[cfg.index('color_dlc')+2],16)]
    afk_time = int(cfg[cfg.index('afk_time')+2])
    cancel_refresh = int(cfg[cfg.index('cancel_refresh')+2])
    update_time = int(cfg[cfg.index('update_time')+2])*60
    channel_allowed = [int(x) for x in cfg[cfg.index('channel_allowed')+2].split(',')]
    exec("channel_allowed="+cfg[cfg.index('channel_allowed')+2])
    return (TOKEN,prefix,check_files,file_expire,url_max_clicks,url_min_clicks,color_embed,show_updates,update_channel,colors_list,afk_time,cancel_refresh,update_time,channel_allowed)

TOKEN,prefix,check_files,file_expire,url_max_clicks,url_min_clicks,color_embed,show_updates,update_channel,colors_list,afk_time,cancel_refresh,update_time,channel_allowed = init_params()
listes={'base':base_list,'update':upd_list,'dlc':dlc_list}
cmd=['update_list','base','update','dlc','restart','update_config','test_mat','test_params']
games=[]

for el in cmd :
    cmd[cmd.index(el)]=prefix+el

games=[]#Will store games names,id,date,url
mat={} #store users using the bot, their dm_channel and choices variable



#######################Discord bot functions##################
bot = commands.Bot(command_prefix=prefix)

@bot.event
async def on_ready():
    print("Logged in as " + bot.user.name)

@bot.command()
async def test(ctx,arg):
    if type(arg) == type('') :
        exec(arg)
    else :
        exec(str(arg))
    await ctx.message.add_reaction('\U00002705')

@bot.command()
async def base(ctx,*args):
    try :
        if ctx.message.channel.id not in channel_allowed :
            ask = discord.Embed(title="Wrong channel",description="You sent the command on the wrong channel !",color=color_embed)
            return(await ctx.author.send(embed=ask))
        
        ask=command(ctx,'base',args)
        await ctx.author.send(embed=ask)
        if "not enough explicit" in ask.description or "Nothing found" in ask.description :
            mat.pop(str(ctx.author))
            await ctx.message.add_reaction('\U000026D4')
        elif 'http' not in ask.description :
            mat[str(ctx.author)]['dm_channel']=ctx.author.dm_channel.id
            mat[str(ctx.author)]['message']=ctx.message
            mat[str(ctx.author)]['datetime']=datetime.datetime.now()
        else :
            await ctx.message.add_reaction('\U00002705')
            
    except discord.errors.Forbidden :
        traceback.print_exc()
        await ctx.message.add_reaction('\U0001F6AB')
    except Exception :
        traceback.print_exc()
        await ctx.message.add_reaction('\U000026D4')
        await ctx.author.send(embed=discord.Embed(title="An error occured, try again",color=color_embed))

@bot.command()
async def update(ctx,*args):
    try :
        if ctx.message.channel.id not in channel_allowed :
            ask = discord.Embed(title="Wrong channel",description="You sent the command on the wrong channel !",color=color_embed)
            return(await ctx.author.send(embed=ask))
        
        ask=command(ctx,'update',args)
        await ctx.author.send(embed=ask)   
        if "not enough explicit" in ask.description or "Nothing found" in ask.description :
            mat.pop(str(ctx.author))
            await ctx.message.add_reaction('\U000026D4')
        elif 'http' not in ask.description :
            mat[str(ctx.author)]['dm_channel']=ctx.author.dm_channel.id
            mat[str(ctx.author)]['message']=ctx.message
            mat[str(ctx.author)]['datetime']=datetime.datetime.now()
        else :
            await ctx.message.add_reaction('\U00002705')
            
    except discord.errors.Forbidden :
        traceback.print_exc()
        await ctx.message.add_reaction('\U0001F6AB')            
    except Exception :
        traceback.print_exc()
        await ctx.message.add_reaction('\U000026D4')
        await ctx.author.send(embed=discord.Embed(title="An error occured, try again",color=color_embed))
    

@bot.command()
async def dlc(ctx,*args): 
    try :
        if ctx.message.channel.id not in channel_allowed :
            ask = discord.Embed(title="Wrong channel",description="You sent the command on the wrong channel !",color=color_embed)
            return(await ctx.author.send(embed=ask))
        
        ask=command(ctx,'dlc',args)
        await ctx.author.send(embed=ask)
        if "not enough explicit" in ask.description or "Nothing found" in ask.description :
            mat.pop(str(ctx.author))
            await ctx.message.add_reaction('\U000026D4')
        elif 'http' not in ask.description :
            mat[str(ctx.author)]['dm_channel']=ctx.author.dm_channel.id
            mat[str(ctx.author)]['message']=ctx.message
            mat[str(ctx.author)]['datetime']=datetime.datetime.now()
        else :
            await ctx.message.add_reaction('\U00002705')
            
    except discord.errors.Forbidden :
        traceback.print_exc()
        await ctx.message.add_reaction('\U0001F6AB')    
    except Exception :
        traceback.print_exc()
        await ctx.message.add_reaction('\U000026D4')
        await ctx.author.send(embed=discord.Embed(title="An error occured, try again",color=color_embed))



def command(ctx,dos,args):
    #if str(ctx.author) not in mat : #if it's the first time of the user or it been more than a day, add user to mat
    mat[str(ctx.author)]={}
    key=''
    for el in args:
        key+=str(el)+' '
    key=key[:len(key)-1]

    argums=[dos,key]
    [choices,desc]=a02_simple_search(argums)
    if len(choices)!=1 :  #if there is no or multiple games found
        mat[str(ctx.author)]['choices']=choices
        name='Choose your game'
    else : #if there is only one game found, dl
        desc=drive_dl(choices[0],service,str(ctx.author)) #create the link for the user
        name=choices[0]['name']
        

    if len(desc)>2000 :
        desc = "Search not enough explicit, please try again"
    ask=discord.Embed(title=name,description=desc,color=color_embed)
    return(ask)    

@bot.event
async def on_message(message): #whenever a message is post
    res=message.content
    if message.author == bot.user: #return if it's from bot
        return(await bot.process_commands(message))



    if str(res).split(' ')[0] in cmd : #message is a command
        return(await bot.process_commands(message))

    found=0
    for el in mat : #search if it's a known user (already in the process of downloading a game)
        try:
            if mat[el]['dm_channel']==message.channel.id :
                usr=str(message.author)
                found=1
        except Exception :
            traceback.print_exc()
            mat[el]={}
            await message.author.send(embed=discord.Embed(title="An error occured, try again",color=color_embed))
            return(await bot.process_commands(message))
            

    if found==0: #if not return
        return(await bot.process_commands(message))
    
    try :
        ask=discord.Embed(title="Working...",description="Looking for your game... \n Please Wait",color=color_embed)
        msg=await message.author.send(embed=ask)
        
        res=int(res)
        name=mat[usr]['choices'][res-1]['name']
        first_message=mat[usr]['message']
        desc=drive_dl(mat[usr]['choices'][res-1],service,usr)
        
        ask=discord.Embed(title=name,description=desc,color=color_embed)
        await msg.edit(embed=ask)
        await first_message.add_reaction('\U00002705')

    except  Exception :
        traceback.print_exc()
        await mat[usr]['message'].add_reaction('\U000026D4')
        mat.pop(usr)
        await message.author.send(embed=discord.Embed(title="An error occured, try again",color=color_embed))
        
    
@bot.command()
async def update_list(ctx):
    if ctx.message.channel.id not in channel_allowed :
        ask = discord.Embed(title="Wrong channel",description="You sent the command on the wrong channel !",color=color_embed)
        return(await ctx.author.send(embed=ask))
        
    embed = await update_list_func()
    await ctx.author.send(embed=embed)

async def update_list_func():
    await bot.change_presence(status=discord.Status.idle)
    global listes
    new_games = update_listes()
    base_list,upd_list,dlc_list=read_listes()
    listes={'base':base_list,'update':upd_list,'dlc':dlc_list}
    
    if show_updates == 1 :
        titles=['New base','New update','New dlc']
        for i in range(0,3) :
            for el in new_games [i] :
                if el !=[] :
                    embed=discord.Embed(title=titles[i],color=colors_list[i],description=el['name'])
                    await bot.get_channel(update_channel).send(embed=embed)
                    time.sleep(1)
    
    await bot.change_presence(status=discord.Status.online)
    embed=discord.Embed(title="Update done",color=color_embed)
    
    return embed
    
    
@bot.command()
async def update_config(ctx):
    global TOKEN,prefix,check_files,file_expire,url_max_clicks,url_min_clicks,color_embed,show_updates,update_channel,colors_list,afk_time,cancel_refresh,update_time,channel_allowed
    if ctx.message.channel.id not in channel_allowed :
        ask = discord.Embed(title="Wrong channel",description="You sent the command on the wrong channel !",color=color_embed)
        return(await ctx.author.send(embed=ask))
        
    
    TOKEN,prefix,check_files,file_expire,url_max_clicks,url_min_clicks,color_embed,show_updates,update_channel,colors_list,afk_time,cancel_refresh,update_time,channel_allowed = init_params()
    embed=discord.Embed(title="Update done",color=color_embed)
    await ctx.author.send(embed=embed)
    
@bot.command()
async def test_mat(ctx):
    embed=discord.Embed(title='mat',description = str(mat),color=color_embed)
    await ctx.author.send(embed=embed)

@bot.command()
async def test_games(ctx):
    embed=discord.Embed(title='game',description = str(games),color=color_embed)
    await ctx.author.send(embed=embed)
    
@bot.command()
async def test_params(ctx):
    embed=discord.Embed(title='config',color=color_embed)
    embed.add_field(name='TOKEN',value=TOKEN)
    embed.add_field(name='prefix',value=prefix)
    embed.add_field(name='check_files',value=check_files)
    embed.add_field(name='file_expire',value=file_expire)
    embed.add_field(name='url_max_clicks',value=url_max_clicks)
    embed.add_field(name='url_min_clicks',value=url_min_clicks)
    embed.add_field(name='color_embed',value=color_embed)
    await ctx.author.send(embed=embed)


bot.remove_command('help')
@bot.command()
async def help(ctx):
    embed=discord.Embed(title='Help',color=color_embed,description='This bot help you download games. DMs need to be active on this server')
    embed.add_field(name=prefix+'help',value='This help command. Show existing commands')
    embed.add_field(name=prefix+'base',value='To download base game.\n ­```\nbase ThumPer```')
    embed.add_field(name=prefix+'update',value='To download update of a game.\n ```\nupdate Baba is you```')
    embed.add_field(name=prefix+'dlc',value='To download dlc of a game.\n ­```\ndlc Breath of the wild```')
    embed.add_field(name='\u200B',value='\u200B')
    embed.add_field(name='Remarks',value="The bot will then answer you by DM. \n If more than one game are found, you'll have to answer the bot by DM with the number of your choice \n Keywords are not case sensitive and you can have multiples keywords. \n When the bot is idle, it's working on something else. Please be patient and don't spam it.")
    
    await ctx.author.send(embed=embed)

@bot.command()
async def restart(ctx):
    if ctx.message.channel.id not in channel_allowed :
        ask = discord.Embed(title="Wrong channel",description="You sent the command on the wrong channel !",color=color_embed)
        return(await ctx.author.send(embed=ask))
        
    python = sys.executable    
    os.execl(python, python, * sys.argv)
    await ctx.author.send(embed=embed)

#####################Utilities functions########################
def a02_simple_search(args): #function to find the game in a list
    dos=listes[args[0]]
    key=args[1]
    num = 1 #the number in front of each element found
    choices = []
    ask='' #ask and choices will depend on what was found
    t_id=''
    for el in dos :
        if key.lower() in el['name'].lower() :
            spl=el['name'].split('[')
            for sp in spl :
                if sp.find(']')==16 :
                    t_id=sp[:-1]
            if t_id not in ask :
                ask+='\n'+str(num)+' - '+el['name']
                choices.append(el)
                num+=1
    
    if num == 1 : #if still at 1, nothing was found
        ask='Nothing found...'
       
    return(choices,ask)

async def delete_mat(): #function to clean afk users in mat variable
    await bot.wait_until_ready()
    while True :
        now=datetime.datetime.now()
        mat_rm=[]
        for el in mat :
            if now-mat[el]['datetime']>datetime.timedelta(seconds=afk_time): #for each user, determine if afk_time seconds passed since begining of the process
                mat_rm.append(el)
       
        for el in mat_rm:
            await mat[el]['message'].add_reaction('\U000026D4')
            mat.pop(el)
        
        await asyncio.sleep(cancel_refresh) #clean bot's mat each cancel_refresh seconds

def short(url) :
    date=(datetime.datetime.now()+datetime.timedelta(days=2)).strftime('%m/%d/%Y')
    data = {'url':url,'uses':url_max_clicks,'expire':date}
    res=rq.get(api_short,params=data)
    return(res.json()['data']['url'])


#####################Gdrive functions##########################
async def delete_drive(): #function to clean bot's drive at regular interval
    await bot.wait_until_ready()
    while True :
        now=datetime.datetime.now()
        games_rm=[]
        for el in games :
            if now-games[games.index(el)]['date']>datetime.timedelta(hours=file_expire): 
                el['service'].files().delete(fileId=el['file_id']).execute()
                games_rm.append(el)

        
            el['service'].files().emptyTrash().execute()
        
        for el in games_rm :
            games.remove(el)#remove all games info in the games variable    
        await asyncio.sleep(check_files) #clean bot's drive each check_files minutes

async def update_timer():
    await bot.wait_until_ready()
    while True :
        useless = await update_list_func()       
        await asyncio.sleep(update_time) #update drive each update_time minutes

def drive_dl(game,service,usr) :
    r_games=games
    r_games.reverse() #reverse games list to search the last same game
    for el in r_games :
        if el['id']==game['id'] :
            link=el['link']
            res=rq.get(api_read,params={'id':link.rsplit('/',1)[1]})
            if int(res.json()['data']['uses'])>url_min_clicks :
                el['date']=datetime.datetime.now()
                return(link)
        
    
    
    ok=0
    body ={'parents':['root']}
    for serv in service :
        try :
            res = serv.files().copy(fileId=game['id'],body=body).execute()
            if res['kind']=='drive#file' :
                ok=1
                game['service']=serv
                break
            
        except Exception as e :
            if e.__dict__['resp']['status']=='403' :
                print('No more space available in '+str(service.index(serv))+' drive.')
                print('Changing backup drive.')
                continue

    if ok==0 :
        raise Exception('No more space available in backup drives')

    game['file_id']=res['id']
        
    body = {'role':'reader','type':'anyone'}
    
    res = game['service'].permissions().create(fileId=game['file_id'],body=body).execute()
    game['perm_id']=res['id']
    
    res = game['service'].files().get(fileId=game['file_id'],fields='webViewLink').execute()
    
    link=short(res['webViewLink'])
    
    game['link']=link
    game['date']=datetime.datetime.now()
    
    games.append(game)
    
    mat.pop(usr) #clean mat
    
    return(link)

bot.loop.create_task(delete_drive())
bot.loop.create_task(delete_mat())
bot.loop.create_task(update_timer())
bot.run(TOKEN)

