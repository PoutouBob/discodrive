import os
import pickle
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
import json
import time

def create_service(acc='1') :
    
    SCOPES = ['https://www.googleapis.com/auth/drive']
    
    creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
    if os.path.exists('config/token'+acc+'.pickle'):
        with open('config/token'+acc+'.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        print('Connect to mirror drive account '+acc)
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'config/credentials'+acc+'.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('config/token'+acc+'.pickle', 'wb') as token:
            pickle.dump(creds, token)
    
    with open('config/token'+acc+'.pickle', 'rb') as token:
        creds = pickle.load(token)
        
    service = build('drive', 'v3', credentials=creds)
    
    print("Drive service "+acc+" created")
    
    return service

def create_listes():
    names=['base','update','dlc']
    
    file=open('config/drive.txt','r')
    config=file.read().split()
    file.close()
    
    id_base=config[2]
    id_upd=config[5]
    id_dlc=config[8]
    
    ids=[id_base,id_upd,id_dlc]
    
    for el in ids :
        dos_id=el
        dos_name=names[ids.index(el)]
        liste=[]
    
        pageToken = ''
        while pageToken is not None :
            results = service[0].files().list(pageSize = 1000,
                                           fields = "nextPageToken, files(name,id)",
                                           q = "'"+dos_id+"' in parents",
                                           pageToken=pageToken).execute()
            liste+=results['files']
            pageToken=results.get('nextPageToken')
        
        txt=open('config/'+dos_name+'.txt','w')
        json.dump(liste,txt)
        txt.close()
        
    #Writing the last created time file
    file=open('config/stash_date.txt','w')        
    for el in ids :
        results = service[0].files().list(pageSize = 1,
                                       fields = "files(modifiedTime)",
                                       q = "'"+el+"' in parents",
                                       orderBy='createdTime desc').execute()
        if len(results['files'])==0 :
            print('error '+names[ids.index(el)]+' folder is empty')
        else :
            file.write(names[ids.index(el)]+' = '+results['files'][0]['modifiedTime']+'\n')
    file.close()


def read_listes():
    base_list = json.load(open('config/base.txt','r'))
    upd_list = json.load(open('config/update.txt','r'))
    dlc_list = json.load(open('config/dlc.txt','r'))
    
    return(base_list,upd_list,dlc_list)

def update_listes():
    listes_old=['','','']
    listes_old[0],listes_old[1],listes_old[2] = read_listes()
    
    names=['base','update','dlc']
    
    file=open('config/drive.txt','r')
    config=file.read().split()
    file.close()
    
    id_base=config[2]
    id_upd=config[5]
    id_dlc=config[8]
    
    ids=[id_base,id_upd,id_dlc]
    
    new_games=[[],[],[]]
        
    for el in ids :
        dos_id=el
        dos_name=names[ids.index(el)]
        liste=[]
        
        pageToken = ''
        while pageToken is not None :
            results = service[0].files().list(pageSize = 1000,
                                           fields = "nextPageToken, files(name,id)",
                                           q = "'"+dos_id+"' in parents",
                                           pageToken=pageToken).execute()
            liste+=results['files']
            pageToken=results.get('nextPageToken')
        
        new=new_games[ids.index(el)]
        old=listes_old[ids.index(el)]
        for x in liste :
            if x not in old :
                new.append(x)
        
        txt=open('config/'+dos_name+'.txt','w')
        json.dump(liste,txt)
        txt.close()        
    
    return(new_games)

    
    
print('Creating mirror drive 1 service')
service=[create_service()]
time.sleep(2)
file=open('config/drive.txt','r')
spl=file.read().split()
file.close()
if 'num_account' in spl :
    num_account=spl[spl.index('num_account')+2]
else : 
    num_account=0
for x in range(int(num_account)):
    print('Creating mirror drive '+str(x+2)+' service')
    service.append(create_service(str(x+2)))
    time.sleep(2)



if 'base.txt' not in os.listdir('config')  or 'dlc.txt' not in os.listdir('config') or 'update.txt' not in os.listdir('config') :
    create_listes()

base_list,upd_list,dlc_list = read_listes()