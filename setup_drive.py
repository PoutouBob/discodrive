import os
import pickle
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow

SCOPES = ['https://www.googleapis.com/auth/drive']

# ########### INIT DRIVE ACCESS
creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
if os.path.exists('config/token1.pickle'):
    with open('config/token1.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    print('Connect to mirror drive account 1')
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'config/credentials1.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('config/token1.pickle', 'wb') as token:
        pickle.dump(creds, token)

with open('config/token1.pickle', 'rb') as token:
    creds = pickle.load(token)
    
service = build('drive', 'v3', credentials=creds)

# ########### HELP YOU CREATE THE DRIVE.TXT IN CONFIG
liste=[]
pageToken=''
while pageToken is not None :
    results = service.files().list(pageSize = 1000,
                                   fields = "nextPageToken, files(name,id)",
                                   q = "mimeType='application/vnd.google-apps.folder'",
                                   pageToken=pageToken).execute()
    liste+=results['files']
    pageToken=results.get('nextPageToken')

num = 1
for el in liste :
    print(str(num)+'- '+el['name']+'\n')
    num+=1

file=open('config/drive.txt','w')

print('\nwhat is your games base folder ?')
file.write('id_base = '+liste[int(input())-1]['id']+'\n')

print('\nwhat is your games updates folder ?')
file.write('id_upd = '+liste[int(input())-1]['id']+'\n')

print('\nwhat is your games dlc folder ?')
file.write('id_dlc = '+liste[int(input())-1]['id']+'\n')

print('How many gdrive mirror account do you have (counting the one you just logged in) ?')
num=input()
if int(num)>0 :
    file.write('num_account = '+str(int(num)-1))

file.close()

input('Setup completed, press a key to close')